import { useEffect } from "react";
import Router from "./Router";
import { getUsers } from "./services/services";

function App() {
  useEffect(() => {
    getUsers();
  }, []);

  return (
    <div className="text-center h-full bg-gray-50">
      <Router />
    </div>
  );
}

export default App;
