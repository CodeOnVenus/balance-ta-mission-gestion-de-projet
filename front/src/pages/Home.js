import React, { useEffect, useState } from "react";
import JobList from "../components/job/JobList";
import NavBar from "../components/navigation/NavBar";
import SearchBar from "../components/SearchBar";
import { getJobs } from "../services/services";

function Home(props) {
  const [jobs, setJobs] = useState(null);

  useEffect(() => {
    const fetchJobs = async () => {
      const result = await getJobs();
      setJobs(result);
    };
    fetchJobs();
  }, []);

  return (
    <div>
      <NavBar />
      <div className="m-auto max-w-6xl">
        <SearchBar />
        <JobList jobs={jobs} />
      </div>
    </div>
  );
}

export default Home;
