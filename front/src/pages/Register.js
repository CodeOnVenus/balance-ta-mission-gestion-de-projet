import RegisterForm from "../components/register/RegisterForm";
import TopInfo from "../components/register/TopInfo";

export default function Register() {
  return (
    <div className="lg:flex justify-center flex-col m-8">
      <TopInfo />
      <RegisterForm />
    </div>
  );
}
