import { useNavigate } from "react-router-dom";

export function Exit(props) {
  const navigate = useNavigate();

  function navigateBack() {
    navigate(-1);
  }
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="1.5em"
      height="1.5em"
      viewBox="0 0 21 21"
      {...props}
      onClick={navigateBack}
      className="cursor-pointer"
    >
      <path
        fill="none"
        stroke="currentColor"
        strokeLinecap="round"
        strokeLinejoin="round"
        d="m7.405 13.5l-2.905-3l2.905-3m-2.905 3h9m-6-7l8 .002c1.104.001 2 .896 2 2v9.995a2 2 0 0 1-2 2l-8 .003"
      ></path>
    </svg>
  );
}
