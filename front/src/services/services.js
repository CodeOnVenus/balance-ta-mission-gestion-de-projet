const { REACT_APP_API_URL } = process.env;

export async function getUsers() {
  const response = await fetch(`${REACT_APP_API_URL}users`, {
    headers: {
      "content-type": "application/ld+json",
    },
  });
  const data = await response.json();
  const users = data["hydra:member"];

  return users;
}

export async function getTechnos() {
  const response = await fetch(`${REACT_APP_API_URL}technos`, {
    headers: {
      "content-type": "application/ld+json",
    },
  });
  const data = await response.json();
  const technos = data["hydra:member"];

  return technos;
}

export async function getJobs() {
  const response = await fetch(`${REACT_APP_API_URL}jobs`, {
    headers: {
      "content-type": "application/ld+json",
    },
  });
  const data = await response.json();
  const jobs = data["hydra:member"];

  return jobs;
}
