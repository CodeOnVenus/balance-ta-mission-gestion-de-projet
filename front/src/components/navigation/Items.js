import { Disclosure } from "@headlessui/react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/outline";

const navItem = `
  inline-flex items-center border-indigo-500 lg:px-5 md:px-2 pb-1
  font-medium text-gray-900 lg:text-lg md:text-base sm:text-sm";
`;

export default function Items() {
  return (
    <Disclosure as="nav" className="bg-white shadow">
      {({ open }) => (
        <>
          <div className="flex justify-center px-4">
            <div className="ml-2 mr-2 flex items-center md:hidden">
              {/* Mobile menu button */}
              <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-indigo-500">
                <span className="sr-only">Open main menu</span>
                {open ? (
                  <XMarkIcon className="block h-6 w-6" aria-hidden="true" />
                ) : (
                  <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                )}
              </Disclosure.Button>
            </div>

            <div className=" md:mx-6 flex md:space-x-2 w-full justify-between">
              {/* Current: "border-indigo-500 text-gray-900", Default: "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700" */}
              <a href="#0" className={navItem}>
                JavaScript
              </a>
              <a href="#1" className={navItem + `border-b-2`}>
                Python
              </a>
              <a href="#2" className={navItem}>
                PHP
              </a>
              <a href="#3" className={navItem}>
                Ruby
              </a>
              <a href="#4" className={navItem}>
                Go
              </a>
              <a href="#5" className={navItem}>
                Swift
              </a>
              <a href="#6" className={navItem}>
                C
              </a>
              <a href="#7" className={navItem}>
                Java
              </a>
              <a href="#8" className={navItem}>
                .Net
              </a>
            </div>
          </div>
        </>
      )}
    </Disclosure>
  );
}
