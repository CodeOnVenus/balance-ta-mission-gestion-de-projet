import { useContext } from "react";
import { Disclosure } from "@headlessui/react";
import { PlusIcon } from "@heroicons/react/20/solid";
import { UserContext } from "../../contexts/userContext";
import Items from "./Items";
import DropdownMenu from "./DropdownMenu";
import { useNavigate } from "react-router-dom";

const navItem = `
  inline-flex items-center border-indigo-500 lg:px-5 md:px-2 pt-1 
  font-medium text-gray-900 lg:text-lg md:text-base sm:text-sm";
`;

const button = `
"relative inline-flex items-center rounded-md border border-transparent bg-indigo-600 px-4 py-2 
lg:text-lg md:text-base sm:text-sm font-medium text-white shadow-sm hover:bg-indigo-700 
focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
`;

export default function NavBar() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  function navigateToLogin() {
    navigate("/login");
  }

  function navigateToPostOpportunity() {
    navigate("/post");
  }

  return (
    <>
      <Disclosure as="nav" className="m-2">
        {({ open }) => (
          <>
            <div className="mx-auto  px-4 sm:px-6 lg:px-8">
              <div className="flex h-16 justify-between">
                <div className="flex flex-shrink-0 items-center">
                  <img
                    className="lg:hidden block h-8 w-auto "
                    src="https://api.iconify.design/ps:slideshare.svg"
                    alt="Your Company"
                  />
                  <h3 className={navItem + "hidden h-8 w-auto lg:block"}>
                    BalanceTaMission
                  </h3>
                </div>
                {user ? (
                  <div className="flex items-center">
                    <div className="flex-shrink-0">
                      <button
                        type="button"
                        className={button}
                        onClick={navigateToPostOpportunity}
                      >
                        <PlusIcon
                          className="-ml-1 mr-2 h-7 w-7"
                          aria-hidden="true"
                        />
                        <span>Déposer une opportunité</span>
                      </button>
                    </div>
                    <DropdownMenu />
                  </div>
                ) : (
                  <div className="flex items-center">
                    <div className="flex-shrink-0">
                      <button
                        type="button"
                        className={button}
                        onClick={navigateToLogin}
                      >
                        <span>Se connecter</span>
                      </button>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </>
        )}
      </Disclosure>
    </>
  );
}
