import { CalendarIcon, ChevronRightIcon } from "@heroicons/react/20/solid";

export default function JobItemTest({ job }) {
  return (
    <li key={job["@id"]} className="hover:bg-gray-50">
      <div className="flex justify-between items-center px-4 py-4 sm:px-6 hover:bg-gray-50">
        <div className="text-left font-normal text-gray-500">
          <div className="flex ">
            <p className="font-medium text-indigo-600">Developpeur frontend </p>
            &nbsp;&nbsp;chez&nbsp;&nbsp;
            <p>Mr Bricolage </p>
          </div>

          <div className="flex items-center text-sm my-2">
            <CalendarIcon
              className="mr-1.5 h-5 w-5 flex-shrink-0 text-gray-400"
              aria-hidden="true"
            />
            <p>A partir du 19 janvier 2023</p>
          </div>
          <div className="flex text-sm ">
            <p>Freelance</p>
            &nbsp;&nbsp;-&nbsp;&nbsp;
            <p>{job.TJM} €/jour</p>
          </div>
        </div>
        <div className="ml-5 flex-shrink-0">
          <ChevronRightIcon
            className="h-5 w-5 text-gray-400"
            aria-hidden="true"
          />
        </div>
      </div>
    </li>
  );
}
