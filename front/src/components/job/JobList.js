import React from "react";
import JobItem from "./JobItem";
import JobItemTest from "./JobItemTest";

const positions = [
  {
    id: 1,
    title: "Back End Developer",
    department: "Engineering",
    closeDate: "2020-01-07",
    closeDateFull: "January 7, 2020",
  },
  {
    id: 2,
    title: "Front End Developer",
    department: "Engineering",
    closeDate: "2020-01-07",
    closeDateFull: "January 7, 2020",
  },
  {
    id: 3,
    title: "User Interface Designer",
    department: "Design",
    closeDate: "2020-01-14",
    closeDateFull: "January 14, 2020",
  },
];

function JobList({ jobs }) {
  console.log(jobs);
  return (
    <div className="px-4 py-5 sm:px-6">
      <div className="overflow-hidden bg-white shadow sm:rounded-md ">
        <ul role="list" className="divide-y divide-gray-200">
          {jobs &&
            jobs.map((job) => <JobItemTest key={job["@id"]} job={job} />)}
        </ul>
        <ul role="list" className="divide-y divide-gray-200">
          {positions.map((position) => (
            <JobItem key={position.id} position={position} />
          ))}
        </ul>
      </div>
    </div>
  );
}

export default JobList;
