import React from "react";
import TechnosSelect from "./TechnosSelect";

function SearchBar(props) {
  return (
    <div className="flex flex-col px-4 sm:px-6">
      <div className="mx-6 mb-6 font-medium ">
        <img
          className="m-auto h-16 "
          src="https://api.iconify.design/ps:slideshare.svg"
          alt="Your Company"
        />
        <p className="m-1">Les meilleures opportunitées de la communauté !</p>
      </div>
      <div className="grid lg:grid-cols-4 sm:grid-cols-1 md:grid-cols-2 grid-rows-2 gap-2 my-4">
        <div className=" ">
          <input
            id="search"
            name="search"
            type="search"
            autoComplete="search"
            placeholder="Search"
            required
            className="block w-full appearance-none rounded border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
          />
        </div>

        <div className=" ">
          <input
            id="location"
            name="location"
            type="location"
            autoComplete="location"
            required
            placeholder="Location"
            className="block w-full appearance-none rounded border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
          />
        </div>

        <div>
          <TechnosSelect />
        </div>

        <div className="">
          <button
            type="submit"
            className="flex w-full m-auto justify-center rounded border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
          >
            Search
          </button>
        </div>
      </div>
    </div>
  );
}

export default SearchBar;
