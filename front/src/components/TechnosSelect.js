import { useEffect, useState } from "react";
import Select from "react-tailwindcss-select";
import { getTechnos } from "../services/services";

const TechnosSelect = () => {
  const [technos, setTechnos] = useState(null);
  const [technosSelected, setTechnosSelected] = useState(null);
  const [options, setOptions] = useState([]);

  useEffect(() => {
    const fetchTechnos = async () => {
      const result = await getTechnos();
      setTechnos(result);
      console.log(result);
    };
    fetchTechnos();
  }, []);

  useEffect(() => {
    const opt = createOptionsArray(technos);
    console.log(opt);
    setOptions(opt);
  }, [technos]);

  function createOptionsArray(array) {
    if (array !== null) {
      const options = array.map((techno) => ({
        value: techno.name,
        label: techno.name,
      }));
      return options;
    }
    return [];
  }

  const handleChange = (value) => {
    console.log("value:", value);
    setTechnosSelected(value);
  };

  return (
    <Select
      value={technosSelected}
      primaryColor={"indigo"}
      onChange={handleChange}
      options={options}
      isMultiple={true}
      placeholder={"Technos..."}
    />
  );
};

export default TechnosSelect;
