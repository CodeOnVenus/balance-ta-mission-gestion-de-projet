import React from "react";

function Question({ text }) {
  return (
    <div className="flex items-center justify-around flex-col">
      <label className="text-base font-medium text-gray-900 mt-4">{text}</label>
      <fieldset className="mt-4">
        <div className="space-y-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-10">
          <div key="yes" className="flex items-center">
            <input
              id="yes"
              name="notification-method"
              type="radio"
              defaultChecked="yes"
              className="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-500"
            />
            <label
              htmlFor="yes"
              className=" ml-3 block text-sm font-medium text-gray-700"
            >
              Yes
            </label>
          </div>
          <div key="no" className="flex items-center">
            <input
              id="no"
              name="notification-method"
              type="radio"
              className="h-4 w-4 border-gray-300 text-indigo-600 focus:ring-indigo-500"
            />
            <label
              htmlFor="yes"
              className="ml-3 block text-sm font-medium text-gray-700"
            >
              No
            </label>
          </div>
        </div>
      </fieldset>
    </div>
  );
}

export default Question;
