import React from "react";

function BottomActions(props) {
  return (
    <div className="space-y-2">
      <div className="flex items-center justify-center">
        <div className="flex items-center m-2">
          <input
            id="remember-me"
            name="remember-me"
            type="checkbox"
            className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
          />
          <label
            htmlFor="remember-me"
            className="ml-2 block text-sm text-gray-900"
          >
            Se souvenir de moi
          </label>
        </div>
      </div>

      <div>
        <button
          type="submit"
          className="flex w-full max-w-xs m-auto justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2"
        >
          S'enregistrer
        </button>
      </div>
      <div className="text-sm text-center m-2">
        Tu as déjà un compte ?&ensp;
        <a
          href="/login"
          className="font-medium text-indigo-600 hover:text-indigo-500"
        >
          Se connecter
        </a>
      </div>
    </div>
  );
}

export default BottomActions;
