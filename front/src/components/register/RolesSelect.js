import { useState } from "react";
import Select from "react-tailwindcss-select";

const options = [
  { value: "developper", label: "Développeur" },
  { value: "recruiter", label: "Recruteur" },
];

const RolesSelect = () => {
  const [role, setRole] = useState(null);

  const handleChange = (value) => {
    setRole(value);
  };

  return (
    <Select
      value={role}
      onChange={handleChange}
      options={options}
      isMultiple={true}
      placeholder={"Rôles..."}
    />
  );
};

export default RolesSelect;
