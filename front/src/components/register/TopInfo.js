import React from "react";
import { useNavigate } from "react-router-dom";

function TopInfo() {
  const navigate = useNavigate();

  function navigateToHome() {
    navigate("/");
  }

  return (
    <div className="sm:mx-auto sm:w-full sm:max-w-md">
      <img
        className="mx-auto h-12 w-auto cursor-pointer"
        src="https://api.iconify.design/ps:slideshare.svg"
        alt="Logo"
        onClick={navigateToHome}
      />
      <h2 className="mt-4 text-center text-3xl font-bold tracking-tight text-gray-900">
        Créer un compte
      </h2>
    </div>
  );
}

export default TopInfo;
