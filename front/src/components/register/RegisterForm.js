import React from "react";
import Question from "./Question";
import RolesSelect from "./RolesSelect";
import TechnosSelect from "../TechnosSelect";
import BottomActions from "./BottomActions";

function RegisterForm() {
  return (
    <form className="m-auto bg-white w-fit  p-8 mt-8 rounded-lg">
      <div className="flex flex-col justify-center md:space-y-6">
        <div className="flex justify-center flex-col md:flex-row ">
          <div className="md:mr-4 my-2">
            <label
              htmlFor="pseudo"
              className="block text-sm font-medium text-gray-700"
            >
              Pseudo
            </label>
            <div className="mt-1">
              <input
                id="pseudo"
                name="pseudo"
                type="pseudo"
                autoComplete="pseudo"
                required
                className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
              />
            </div>
          </div>

          <div className="md:ml-4 my-2">
            <label
              htmlFor="email"
              className="block text-sm font-medium text-gray-700"
            >
              Adresse email
            </label>
            <div className="mt-1">
              <input
                id="email"
                name="email"
                type="email"
                autoComplete="email"
                required
                className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
              />
            </div>
          </div>
        </div>

        <div className="flex justify-center flex-col md:flex-row">
          <div className="md:mr-4 my-2">
            <label
              htmlFor="password"
              className="block text-sm font-medium text-gray-700"
            >
              Mot de passe
            </label>
            <div className="mt-1">
              <input
                id="password"
                name="password"
                type="password"
                autoComplete="current-password"
                required
                className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
              />
            </div>
          </div>

          <div className="md:ml-4 my-2">
            <label
              htmlFor="password"
              className="block text-sm font-medium text-gray-700"
            >
              Confirmation du mot de passe
            </label>
            <div className="mt-1">
              <input
                id="password"
                name="password"
                type="password"
                autoComplete="current-password"
                required
                className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-indigo-500 focus:outline-none focus:ring-indigo-500 sm:text-sm"
              />
            </div>
          </div>
        </div>

        <div className="flex justify-center flex-col md:flex-row">
          <div className="md:mr-4 my-2 min-w-[305px]">
            <label
              htmlFor="location"
              className="block text-sm font-medium text-gray-700 py-1"
            >
              Technos
            </label>
            <TechnosSelect />
          </div>

          <div className="md:ml-4 my-2 min-w-[305px]">
            <label
              htmlFor="location"
              className="block text-sm font-medium text-gray-700 py-1"
            >
              Rôles
            </label>
            <RolesSelect />
          </div>
        </div>

        <div className="flex justify-center flex-col md:flex-row ">
          <div className="md:mr-4 my-2 min-w-[305px]">
            <Question text="As-tu un travail ?" />
          </div>

          <div className="md:ml-4 my-2 min-w-[305px]">
            <Question text="Recherches-tu un travail ?" />
          </div>
        </div>
      </div>
      <BottomActions />
    </form>
  );
}

export default RegisterForm;
