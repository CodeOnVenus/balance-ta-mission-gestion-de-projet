# Balance ta mission :)

Un petit readme pour expliquer à ceux qui viendront en cours de route comment ils peuvent reprendre le projet :)

## Pour lancer le build docker :

Dans le dossier api, lance la commande :

```zsh
docker volume create --name=db_balance_ta_mission
```

Ensuite si c'est la première fois dans le dossier parent:

```zsh
docker-compose up --build
```

Sinon :

```zsh
docker-compose up
```

## Installer les dépendances symfony
(Maintenant se fait automatiquement au build, mais si besoin)
```shell
docker exec -it balancetamission-back-1 composer install
```
## Accéder à la database :

```shell
docker exec -it databse_btm bash
psql -U main -h database_btm -d app
```

## Listes des endpoints

Pour accéder à la doc en ligne (une fois les docker lancés): http://localhost:8741/api

TECHNO : 
GET (collections) : https://127.0.0.1:8741/api/technos?page=1
POST/PUT/DELETE : https://127.0.0.1:8000/api/technos
GET (id) : https://127.0.0.1:8000/api/technos/{id}

Recruiter:
GET (collections) : https://127.0.0.1:8741/api/recruiter?page=1
POST/PUT/DELETE : https://127.0.0.1:8000/api/recruiter
GET (id) : https://127.0.0.1:8000/api/recruiter/{id}

User : 
GET (collections) : https://127.0.0.1:8741/api/user?page=1


# Tests

## Tests back (phpunit)

`docker exec -it balancetamission-back-1 php bin/phpunit tests`