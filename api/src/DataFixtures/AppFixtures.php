<?php

namespace App\DataFixtures;

use App\Entity\Job;
use App\Entity\User;
use App\Entity\Techno;
use Symfony\Flex\Recipe;
use App\Entity\Recruiter;
use DateTimeImmutable;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create('fr_FR');

        for ($i = 0; $i < 3; $i++) {
            $user = new User();
            $user->setEmail($faker->email);
            $user->setName($faker->firstName);
            $user->setPassword($faker->password);
            $user->setPseudo($faker->name);
            $user->setHasAJob($faker->boolean);
            $user->setWantAJob($faker->boolean);
            $user->setRoles(["ROLE_USER"]);
            $manager->persist($user);
            for ($j = 0; $j < 3; $j++) {
                $techno = new Techno();
                $techno->setName($faker->name);
                $techno->setTags($faker->words(3));
                $techno->setType($faker->word);
                $manager->persist($techno);
                for ($k = 0; $k < 2; $k++) {
                    $recrutier = new Recruiter();
                    $recrutier->setName($faker->name);
                    $recrutier->setCompany($faker->company);
                    $recrutier->setLinkedinLink($faker->url);
                    $manager->persist($recrutier);
                    for ($l = 0; $l < 10; $l++) {
                        $job = (new Job())
                            ->setDescription($faker->text)
                            ->setRecruiter($recrutier)
                            ->addTechno($techno)
                            ->setReportBy($user)
                            ->setCreatedAt(new DateTimeImmutable($faker->date))
                            ->setReward($faker->randomNumber(2))
                            ->setContact($faker->email)
                            ->setLocalisation($faker->city)
                            ->setTJM($faker->randomNumber(2));
                        $manager->persist($job);
                    }
                }
            }
        }

        //Create Techno only for test :
        for ($i = 1; $i < 10; $i++) {
            $techno = new Techno();
            $techno->setName("TechnoTest$i");
            $techno->setTags(["TagTest1-$i", "TagTest2-$i"]);
            $techno->setType("backend");
            $manager->persist($techno);
        }

        $manager->flush();
    }
}
