<?php

namespace App\Entity;

use App\Entity\Job;
use App\Entity\User;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\Repository\RecruiterRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ApiResource(
    operations: [
        new GetCollection(normalizationContext: ['groups' => ['recru:read']],
    ),
        new Post(),
        new Get(normalizationContext: ['groups' => ['one_recru:read']]),
        new Patch(),
        new Delete(),
    ],
    denormalizationContext: ['groups' => ['recru:write']],
    paginationItemsPerPage: 5,
)]

#[ORM\Entity(repositoryClass: RecruiterRepository::class)]
class Recruiter
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[Groups(['recru:read', 'recru:write', 'one_job:read'])]
    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[Groups(['one_recru:read', 'recru:write', 'one_job:read'])]
    #[ORM\Column(length: 255)]
    private ?string $company = null;

    #[Groups(['one_recru:read','recru:read', 'recru:write', 'one_job:read'])]
    #[ORM\Column(length: 255)]
    private ?string $linkedinLink = null;

    #[Groups(['one_recru:read'])]
    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'recommendedRecuiters')]
    private Collection $recommendBy;

    #[Groups(['one_recru:read', 'recru:write', 'one_job:read'])]
    #[ORM\OneToMany(mappedBy: 'recruiter', targetEntity: Job::class)]
    private Collection $jobs;

    public function __construct()
    {
        $this->recommendBy = new ArrayCollection();
        $this->jobs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getLinkedinLink(): ?string
    {
        return $this->linkedinLink;
    }

    public function setLinkedinLink(string $linkedinLink): self
    {
        $this->linkedinLink = $linkedinLink;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getRecommendBy(): Collection
    {
        return $this->recommendBy;
    }

    public function addRecommendBy(User $recommendBy): self
    {
        if (!$this->recommendBy->contains($recommendBy)) {
            $this->recommendBy->add($recommendBy);
        }

        return $this;
    }

    public function removeRecommendBy(User $recommendBy): self
    {
        $this->recommendBy->removeElement($recommendBy);

        return $this;
    }

    /**
     * @return Collection<int, Job>
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob(Job $job): self
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs->add($job);
            $job->setRecruiter($this);
        }

        return $this;
    }

    public function removeJob(Job $job): self
    {
        if ($this->jobs->removeElement($job)) {
            // set the owning side to null (unless already changed)
            if ($job->getRecruiter() === $this) {
                $job->setRecruiter(null);
            }
        }

        return $this;
    }
}
