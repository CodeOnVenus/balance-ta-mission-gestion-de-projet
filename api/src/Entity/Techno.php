<?php

namespace App\Entity;

use App\Entity\Job;
use App\Entity\User;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\ApiFilter;
use App\Repository\TechnoRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

#[
    ApiResource(
        operations: [
            new GetCollection(normalizationContext: ['groups' => ['techno:read']],),
            new Post(),
            new Get(normalizationContext: ['groups' => ['one_techno:read']]),
            new Patch(),
            new Delete(),
        ],
        denormalizationContext: ['groups' => ['techno:write']],
        paginationItemsPerPage: 5,
    ),
    ApiFilter(SearchFilter::class, properties: ['name' => 'exact', 'type' => 'partial']) // on filtre les technos par nom
]

#[ORM\Entity(repositoryClass: TechnoRepository::class)]
class Techno
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[Groups(['techno:read', 'techno:write', 'jobs:read', 'one_job:read'])]
    #[ORM\Column(length: 255)]
    private string $name;

    #[Groups(['one_techno:read', 'techno:write', 'jobs:read', 'one_job:read'])]
    #[ORM\Column(type: Types::JSON, nullable: true)]
    private array $tags = [];

    #[Groups(['techno:read', 'techno:write', 'jobs:read', 'one_job:read'])]
    #[ORM\Column(length: 255)]
    private ?string $type = null;

    #[ORM\ManyToMany(targetEntity: User::class, mappedBy: 'technos')]
    private Collection $users;

    #[Groups(['one_techno:read', 'techno:write'])]
    #[ORM\ManyToMany(targetEntity: Job::class, mappedBy: 'technos')]
    private Collection $jobs;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->jobs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(?array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users->add($user);
            $user->addTechno($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            $user->removeTechno($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Job>
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob(Job $job): self
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs->add($job);
            $job->addTechno($this);
        }

        return $this;
    }

    public function removeJob(Job $job): self
    {
        if ($this->jobs->removeElement($job)) {
            $job->removeTechno($this);
        }

        return $this;
    }
}
