<?php

namespace App\Entity;

use DateTimeImmutable;
use App\Entity\Recruiter;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\Post;
use Doctrine\DBAL\Types\Types;
use ApiPlatform\Metadata\Patch;
use ApiPlatform\Metadata\Delete;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\JobRepository;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use Symfony\Component\Serializer\Annotation\Groups;

#[
    ApiResource(
        operations: [
            new GetCollection(
                normalizationContext: ['groups' => ['jobs:read']]
            ),
            new Post(),
            new Get(
                normalizationContext: ['groups' => ['one_job:read']]
            ),
            new Patch(),
            new Delete(),
        ],
        denormalizationContext: ['groups' => ['job:write']],
        paginationItemsPerPage: 15,
    ),
    ApiFilter(SearchFilter::class, properties: ['localisation' => 'partial', 'technos.name' => 'partial'])
]
#[ORM\Entity(repositoryClass: JobRepository::class)]
class Job
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['jobs:read', 'one_job:read', 'job:write'])]
    private string $description;

    #[ORM\Column]
    #[Groups(['jobs:read', 'one_job:read', 'job:write'])]
    private ?int $TJM = null;

    #[ORM\Column(nullable: true)]
    #[Groups(['jobs:read', 'one_job:read', 'job:write'])]
    private ?\DateTimeImmutable $beginAt = null;

    #[ORM\Column]
    #[Groups(['jobs:read', 'one_job:read'])]
    private \DateTimeImmutable $createdAt;

    #[ORM\ManyToOne(inversedBy: 'jobsRepport')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['one_job:read', 'job:write'])]
    private User $reportBy;

    #[Groups(['jobs:read', 'one_job:read'])]
    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'jobsApply')]
    private Collection $participants;

    #[Groups(['one_job:read', 'job:write'])]
    #[ORM\Column(length: 255, nullable: true)]
    private ?string $reward = null;

    #[Groups(['one_job:read', 'job:write'])]
    #[ORM\Column(length: 255)]
    private string $contact;

    #[Groups(['one_job:read', 'job:write'])]
    #[ORM\ManyToOne(inversedBy: 'jobs')]
    private ?Recruiter $recruiter = null;

    #[Groups(['jobs:read', 'one_job:read', 'job:write'])]
    #[ORM\ManyToMany(targetEntity: Techno::class, inversedBy: 'jobs')]
    private Collection $technos;

    #[Groups(['jobs:read', 'one_job:read', 'job:write'])]
    #[ORM\Column(length: 255)]
    private ?string $localisation = null;

    public function __construct()
    {
        $this->participants = new ArrayCollection();
        $this->technos = new ArrayCollection();
        $this->createdAt = new DateTimeImmutable("now");
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTJM(): ?int
    {
        return $this->TJM;
    }

    public function setTJM(int $TJM): self
    {
        $this->TJM = $TJM;

        return $this;
    }

    public function getBeginAt(): ?\DateTimeImmutable
    {
        return $this->beginAt;
    }

    public function setBeginAt(?\DateTimeImmutable $beginAt): self
    {
        $this->beginAt = $beginAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getReportBy(): ?User
    {
        return $this->reportBy;
    }

    public function setReportBy(?User $reportBy): self
    {
        $this->reportBy = $reportBy;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }

    public function addParticipant(User $participant): self
    {
        if (!$this->participants->contains($participant)) {
            $this->participants->add($participant);
        }

        return $this;
    }

    public function removeParticipant(User $participant): self
    {
        $this->participants->removeElement($participant);

        return $this;
    }

    public function getReward(): ?string
    {
        return $this->reward;
    }

    public function setReward(?string $reward): self
    {
        $this->reward = $reward;

        return $this;
    }

    public function getContact(): string
    {
        return $this->contact;
    }

    public function setContact(?string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    public function getRecruiter(): ?Recruiter
    {
        return $this->recruiter;
    }

    public function setRecruiter(?Recruiter $recruiter): self
    {
        $this->recruiter = $recruiter;

        return $this;
    }

    /**
     * @return Collection<int, Techno>
     */
    public function getTechnos(): Collection
    {
        return $this->technos;
    }

    public function addTechno(Techno $techno): self
    {
        if (!$this->technos->contains($techno)) {
            $this->technos->add($techno);
        }

        return $this;
    }

    public function removeTechno(Techno $techno): self
    {
        $this->technos->removeElement($techno);

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(string $localisation): self
    {
        $this->localisation = $localisation;

        return $this;
    }
}
