<?php

namespace App\Entity;

use ApiPlatform\Metadata\Get;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UserRepository;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

#[ApiResource(
    operations: [
        new GetCollection(normalizationContext: ['groups' => ['user:read']]),
        new Get(normalizationContext: ['groups' => ['one_user:read']]),
    ],
    paginationItemsPerPage: 5,
)]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(['one_user:read'])]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    #[Groups(['one_job:read', 'one_user:read'])]
    private ?string $email = null;

    #[Groups(['one_user:read'])]
    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column(length: 255)]
    #[Groups(['jobs:read', 'one_job:read', 'one_user:read'])]
    private ?string $pseudo = null;

    #[ORM\Column]
    #[Groups(['one_user:read'])]
    private ?bool $hasAJob = null;

    #[ORM\Column]
    #[Groups(['one_user:read'])]
    private ?bool $wantAJob = null;

    #[ORM\OneToMany(mappedBy: 'reportBy', targetEntity: Job::class)]
    private Collection $jobsRepport;

    #[Groups(['one_job:read'])]
    #[ORM\ManyToMany(mappedBy: 'participants', targetEntity: Job::class)]
    private Collection $jobsApply;

    #[ORM\ManyToMany(targetEntity: Recruiter::class, mappedBy: 'recommendBy')]
    private Collection $recommendedRecuiters;

    #[ORM\ManyToMany(targetEntity: Techno::class, inversedBy: 'users')]
    private Collection $technos;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(type: 'boolean')]
    private $isVerified = false;

    public function __construct()
    {
        $this->jobsApply = new ArrayCollection();
        $this->jobsRepport = new ArrayCollection();
        $this->recommendedRecuiters = new ArrayCollection();
        $this->technos = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function isHasAJob(): ?bool
    {
        return $this->hasAJob;
    }

    public function setHasAJob(bool $hasAJob): self
    {
        $this->hasAJob = $hasAJob;

        return $this;
    }

    public function isWantAJob(): ?bool
    {
        return $this->wantAJob;
    }

    public function setWantAJob(bool $wantAJob): self
    {
        $this->wantAJob = $wantAJob;

        return $this;
    }

    /**
     * @return Collection<int, Job>
     */
    public function getJobsRepport(): Collection
    {
        return $this->jobsRepport;
    }

    public function addJobRepport(Job $job): self
    {
        if (!$this->jobsRepport->contains($job)) {
            $this->jobsRepport->add($job);
            $job->setReportBy($this);
        }

        return $this;
    }

    public function removeJobRepport(Job $job): self
    {
        if ($this->jobsRepport->removeElement($job)) {
            // set the owning side to null (unless already changed)
            if ($job->getReportBy() === $this) {
                $job->setReportBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Recruiter>
     */
    public function getRecommendedRecuiters(): Collection
    {
        return $this->recommendedRecuiters;
    }

    public function addRecommendedRecuiter(Recruiter $recommendedRecuiter): self
    {
        if (!$this->recommendedRecuiters->contains($recommendedRecuiter)) {
            $this->recommendedRecuiters->add($recommendedRecuiter);
            $recommendedRecuiter->addRecommendBy($this);
        }

        return $this;
    }

    public function removeRecommendedRecuiter(Recruiter $recommendedRecuiter): self
    {
        if ($this->recommendedRecuiters->removeElement($recommendedRecuiter)) {
            $recommendedRecuiter->removeRecommendBy($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, Techno>
     */
    public function getTechnos(): Collection
    {
        return $this->technos;
    }

    public function addTechno(Techno $techno): self
    {
        if (!$this->technos->contains($techno)) {
            $this->technos->add($techno);
        }

        return $this;
    }

    public function removeTechno(Techno $techno): self
    {
        $this->technos->removeElement($techno);

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }
}
