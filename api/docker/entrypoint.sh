#!/bin/sh
composer install
php bin/console d:d:c --if-not-exists
php bin/console d:m:m --no-interaction
php bin/console d:f:l --no-interaction
exec apache2-foreground