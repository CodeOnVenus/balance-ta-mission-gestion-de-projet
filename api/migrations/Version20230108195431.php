<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230108195431 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE job_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE recruiter_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE techno_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE job (id INT NOT NULL, report_by_id INT NOT NULL, recruiter_id INT DEFAULT NULL, description TEXT NOT NULL, tjm INT NOT NULL, begin_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, reward VARCHAR(255) DEFAULT NULL, contact VARCHAR(255) NOT NULL, localisation VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_FBD8E0F880FB277D ON job (report_by_id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F8156BE243 ON job (recruiter_id)');
        $this->addSql('COMMENT ON COLUMN job.begin_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN job.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE job_user (job_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(job_id, user_id))');
        $this->addSql('CREATE INDEX IDX_A5FA008BE04EA9 ON job_user (job_id)');
        $this->addSql('CREATE INDEX IDX_A5FA008A76ED395 ON job_user (user_id)');
        $this->addSql('CREATE TABLE job_techno (job_id INT NOT NULL, techno_id INT NOT NULL, PRIMARY KEY(job_id, techno_id))');
        $this->addSql('CREATE INDEX IDX_6983C35DBE04EA9 ON job_techno (job_id)');
        $this->addSql('CREATE INDEX IDX_6983C35D51F3C1BC ON job_techno (techno_id)');
        $this->addSql('CREATE TABLE recruiter (id INT NOT NULL, name VARCHAR(255) NOT NULL, company VARCHAR(255) NOT NULL, linkedin_link VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE recruiter_user (recruiter_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(recruiter_id, user_id))');
        $this->addSql('CREATE INDEX IDX_71AE4A26156BE243 ON recruiter_user (recruiter_id)');
        $this->addSql('CREATE INDEX IDX_71AE4A26A76ED395 ON recruiter_user (user_id)');
        $this->addSql('CREATE TABLE techno (id INT NOT NULL, name VARCHAR(255) NOT NULL, tags JSON DEFAULT NULL, type VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, pseudo VARCHAR(255) NOT NULL, has_ajob BOOLEAN NOT NULL, want_ajob BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE TABLE user_techno (user_id INT NOT NULL, techno_id INT NOT NULL, PRIMARY KEY(user_id, techno_id))');
        $this->addSql('CREATE INDEX IDX_5CD5F5E1A76ED395 ON user_techno (user_id)');
        $this->addSql('CREATE INDEX IDX_5CD5F5E151F3C1BC ON user_techno (techno_id)');
        $this->addSql('CREATE TABLE messenger_messages (id BIGSERIAL NOT NULL, body TEXT NOT NULL, headers TEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, available_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, delivered_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_75EA56E0FB7336F0 ON messenger_messages (queue_name)');
        $this->addSql('CREATE INDEX IDX_75EA56E0E3BD61CE ON messenger_messages (available_at)');
        $this->addSql('CREATE INDEX IDX_75EA56E016BA31DB ON messenger_messages (delivered_at)');
        $this->addSql('CREATE OR REPLACE FUNCTION notify_messenger_messages() RETURNS TRIGGER AS $$
            BEGIN
                PERFORM pg_notify(\'messenger_messages\', NEW.queue_name::text);
                RETURN NEW;
            END;
        $$ LANGUAGE plpgsql;');
        $this->addSql('DROP TRIGGER IF EXISTS notify_trigger ON messenger_messages;');
        $this->addSql('CREATE TRIGGER notify_trigger AFTER INSERT OR UPDATE ON messenger_messages FOR EACH ROW EXECUTE PROCEDURE notify_messenger_messages();');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F880FB277D FOREIGN KEY (report_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F8156BE243 FOREIGN KEY (recruiter_id) REFERENCES recruiter (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE job_user ADD CONSTRAINT FK_A5FA008BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE job_user ADD CONSTRAINT FK_A5FA008A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE job_techno ADD CONSTRAINT FK_6983C35DBE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE job_techno ADD CONSTRAINT FK_6983C35D51F3C1BC FOREIGN KEY (techno_id) REFERENCES techno (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recruiter_user ADD CONSTRAINT FK_71AE4A26156BE243 FOREIGN KEY (recruiter_id) REFERENCES recruiter (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE recruiter_user ADD CONSTRAINT FK_71AE4A26A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_techno ADD CONSTRAINT FK_5CD5F5E1A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_techno ADD CONSTRAINT FK_5CD5F5E151F3C1BC FOREIGN KEY (techno_id) REFERENCES techno (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE job_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE recruiter_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE techno_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('ALTER TABLE job DROP CONSTRAINT FK_FBD8E0F880FB277D');
        $this->addSql('ALTER TABLE job DROP CONSTRAINT FK_FBD8E0F8156BE243');
        $this->addSql('ALTER TABLE job_user DROP CONSTRAINT FK_A5FA008BE04EA9');
        $this->addSql('ALTER TABLE job_user DROP CONSTRAINT FK_A5FA008A76ED395');
        $this->addSql('ALTER TABLE job_techno DROP CONSTRAINT FK_6983C35DBE04EA9');
        $this->addSql('ALTER TABLE job_techno DROP CONSTRAINT FK_6983C35D51F3C1BC');
        $this->addSql('ALTER TABLE recruiter_user DROP CONSTRAINT FK_71AE4A26156BE243');
        $this->addSql('ALTER TABLE recruiter_user DROP CONSTRAINT FK_71AE4A26A76ED395');
        $this->addSql('ALTER TABLE user_techno DROP CONSTRAINT FK_5CD5F5E1A76ED395');
        $this->addSql('ALTER TABLE user_techno DROP CONSTRAINT FK_5CD5F5E151F3C1BC');
        $this->addSql('DROP TABLE job');
        $this->addSql('DROP TABLE job_user');
        $this->addSql('DROP TABLE job_techno');
        $this->addSql('DROP TABLE recruiter');
        $this->addSql('DROP TABLE recruiter_user');
        $this->addSql('DROP TABLE techno');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE user_techno');
        $this->addSql('DROP TABLE messenger_messages');
    }
}
