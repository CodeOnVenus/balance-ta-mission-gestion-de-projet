<?php

namespace App\Tests\ApiTest;

use App\Entity\Techno;
use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;
use App\Repository\TechnoRepository;
use PHPUnit\Framework\Exception;

use function PHPUnit\Framework\throwException;

class EndpointTechnoTest extends ApiTestCase
{

    public function testGetCollection(): void
    {
        // The client implements Symfony HttpClient's `HttpClientInterface`, and the response `ResponseInterface`
        $response = static::createClient()->request('GET', '/api/technos');

        $this->assertResponseIsSuccessful();
        // Asserts that the returned content type is JSON-LD (the default)
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        // Asserts that the returned JSON is a superset of this one
        $this->assertJsonContains([
            '@context' => '/api/contexts/Techno',
            '@id' => '/api/technos',
            '@type' => 'hydra:Collection',
            // 'hydra:totalItems' => 9, TODO quand on aura une base de test reload à chaque étape
            'hydra:view' => [
                '@id' => '/api/technos?page=1',
                '@type' => 'hydra:PartialCollectionView',
                'hydra:first' => '/api/technos?page=1',
                // 'hydra:last' => '/api/technos?page=2', TODO quand on aura une base de test reload à chaque étape
                'hydra:next' => '/api/technos?page=2',
            ],
        ]);

        // Because test fixtures are automatically loaded between each test, you can assert on them
        $this->assertCount(5, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(Techno::class);
    }

    public function testCreateTechno(): void
    {
        $response = static::createClient()->request('POST', '/api/technos', ['json' => [
            'name' => 'maTechno',
            'tags' => ["tag 1", "tag 2"],
            'type' => 'Backend',
            'jobs' => [],

        ]]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains([
            '@context' => '/api/contexts/Techno',
            '@type' => 'Techno',
            'name' => 'maTechno',
            'tags' => ["tag 1", "tag 2"],
            'type' => 'Backend',
            'jobs' => [],
        ]);
        $this->assertMatchesRegularExpression('~^/api/technos/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Techno::class);
    }

    public function testCreateInvalidTechno(): void
    {
        static::createClient()->request('POST', '/api/technos', ['json' => [
            'name' => 12,
        ]]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        //Peut être modifié si on veut vérifier des erreus de constraint.
        $this->assertJsonContains([
            '@context' => '/api/contexts/Error',
            '@type' => 'hydra:Error',
            'hydra:title' => 'An error occurred',
            'hydra:description' => "The type of the \"name\" attribute must be \"string\", \"integer\" given.",
        ]);
    }

    public function testUpdateTechno(): void
    {
        $client = static::createClient();

        $technoRepository = static::getContainer()->get('doctrine')->getRepository(Techno::class);

        //TODO Update when we got a test database and we refresh it
        $techno = null;
        $i = 1;
        while ($techno === null) {
            $techno = $technoRepository->findOneBy(["name" => "TechnoTest$i"]);
            $i++;
            if ($i >= 10) {
                throw new Exception("Il faut recharger les fixtures");
                break;
            }
        }


        $client->request('PATCH', "/api/technos/" . $techno->getId(), [
            'json' => [
                'type' => 'frontend'
            ],
            'headers' => [
                "Content-Type" => "application/merge-patch+json"
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            '@id' => "/api/technos/" . $techno->getId(),
            'type' => 'frontend'
        ]);
    }

    public function testDeleteTechnos(): void
    {
        $client = static::createClient();

        $technoRepository = static::getContainer()->get('doctrine')->getRepository(Techno::class);

        //TODO Update when we got a test database and we refresh it
        $techno = null;
        $i = 1;
        while ($techno === null) {
            $techno = $technoRepository->findOneBy(["name" => "TechnoTest$i"]);
            $i++;
            if ($i >= 10) {
                throw new Exception("Il faut recharger les fixtures");
                break;
            }
        }


        $client->request('DELETE', "/api/technos/" . $techno->getId());

        $this->assertResponseStatusCodeSame(204);
        $this->assertNull(
            $technoRepository->findOneBy(["name" => "TechnoTest2"])
        );
    }
}
