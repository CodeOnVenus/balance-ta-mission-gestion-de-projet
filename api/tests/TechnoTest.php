<?php

namespace App\Tests;

use App\Entity\Job;
use App\Entity\User;
use App\Entity\Techno;
use PHPUnit\Framework\TestCase;

class TechnoTest extends TestCase
{
    public function testSomething(): void
    {
        $techno = (new Techno())
            ->setName('name')
            ->setTags(['tag1', 'tag2'])
            ->setType('type');

        $this->assertEquals('name', $techno->getName());
        $this->assertEquals(['tag1', 'tag2'], $techno->getTags());
        $this->assertEquals('type', $techno->getType());
    }

    public function testTechnoEntityHasUser(): void
    {
        $user = (new User())
            ->setEmail('email')
            ->setPassword('password')
            ->setRoles(['ROLE_USER']);

        $techno = (new Techno())
            ->setName('name')
            ->setTags(['tag1', 'tag2'])
            ->setType('type')
            ->addUser($user);

        $this->assertEquals('email', $techno->getUsers()[0]->getEmail());
        $this->assertEquals('password', $techno->getUsers()[0]->getPassword());
        $this->assertEquals(['ROLE_USER'], $techno->getUsers()[0]->getRoles());
    }

    public function testTechnoIsEmpty()
    {
        $techno = new Techno();
        $this->assertEmpty($techno->getUsers());
        $this->assertEmpty($techno->getJobs());
        $this->assertEmpty($techno->getTags());
        $this->assertEmpty($techno->getType());
    }

    public function testTechnoAddJob()
    {
        $job = (new Job())
            ->setDescription('description')
            ->setTJM(1000)
            ->setBeginAt(new \DateTimeImmutable("2021-01-01"))
            ->setCreatedAt(new \DateTimeImmutable("2021-01-01"))
            ->setReward('reward')
            ->setContact('contact')
            ->setLocalisation('localisation');

        $techno = (new Techno())
            ->setName('name')
            ->setTags(['tag1', 'tag2'])
            ->setType('type')
            ->addJob($job);

        $this->assertEquals('description', $techno->getJobs()[0]->getDescription());
        $this->assertEquals(1000, $techno->getJobs()[0]->getTJM());
        $this->assertEquals(new \DateTimeImmutable("2021-01-01"), $techno->getJobs()[0]->getBeginAt());
        $this->assertEquals(new \DateTimeImmutable("2021-01-01"), $techno->getJobs()[0]->getCreatedAt());
        $this->assertEquals('reward', $techno->getJobs()[0]->getReward());
        $this->assertEquals('contact', $techno->getJobs()[0]->getContact());
        $this->assertEquals('localisation', $techno->getJobs()[0]->getLocalisation());
        $this->assertEquals($techno->getJobs()[0], $job);
    }
}
