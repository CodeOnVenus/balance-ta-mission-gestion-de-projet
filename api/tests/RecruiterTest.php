<?php

namespace App\Tests;

use App\Entity\Job;
use App\Entity\User;
use App\Entity\Recruiter;
use PHPUnit\Framework\TestCase;

class RecruiterTest extends TestCase
{
    public function testRecruiterEntity(): void
    {
        $user = (new User())
            ->setEmail('email')
            ->setPassword('password')
            ->setRoles(['ROLE_USER']);

        $job = (new Job())
                ->setDescription('description')
                ->setTJM(1000)
                ->setBeginAt(new \DateTimeImmutable("2021-01-01"))
                ->setCreatedAt(new \DateTimeImmutable("2021-01-01"))
                ->setReward('reward')
                ->setContact('contact')
                ->setLocalisation('localisation');


        $recruiter = (new Recruiter())
                ->setName('name')
                ->setCompany('company')
                ->setLinkedinLink('https://www.linkedin.com/in/firstname-lastname-123456789/')
                ->addRecommendBy($user)
                ->addJob($job);

        $this->assertEquals('name', $recruiter->getName());
        $this->assertEquals('company', $recruiter->getCompany());
        $this->assertEquals('https://www.linkedin.com/in/firstname-lastname-123456789/', $recruiter->getLinkedinLink());
        $this->assertEquals($user, $recruiter->getRecommendBy()[0]);
        $this->assertEquals($job, $recruiter->getJobs()[0]);
    }
}
