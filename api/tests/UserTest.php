<?php

namespace App\Tests;

use App\Entity\User;
use App\Entity\Techno;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserTest extends TestCase
{
    private $encoder;
    public function setUp(): void
    {
        $this->encoder = $this->createMock(UserPasswordHasher::class);
    }
    public function testHashPassword(): void
    {
        $user = new User();
        $plainPassword = 'password';
        $hashPassword = $this->encoder->hashPassword($user, $plainPassword);
        $user->setPassword($hashPassword);
        $user->setEmail('user@email.fr');
        $user->setRoles(['ROLE_USER']);
        $plainPassword = 'password';
        $hashPassword = $this->encoder->hashPassword($user, $plainPassword);
        $user->setPassword($hashPassword);

        $this->assertNotEquals('password', $user->getPassword());
        $this->assertEquals($hashPassword, $user->getPassword());
    }

    public function testUserEntityHasTechno(): void
    {

        $techno = (new Techno())
            ->setName('name')
            ->setTags(['tag1', 'tag2'])
            ->setType('type');

        $user = (new User())
            ->setEmail('email')
            ->setRoles(['ROLE_USER'])
            ->setPassword('password')
            ->addTechno($techno);

        $this->assertEquals('name', $user->getTechnos()[0]->getName());
        $this->assertEquals(['tag1', 'tag2'], $user->getTechnos()[0]->getTags());
        $this->assertEquals('type', $user->getTechnos()[0]->getType());
    }

    public function testUserIsEmpty()
    {
        $user = new User();
        $this->assertEmpty($user->getTechnos());
        $this->assertEmpty($user->getEmail());
    }

    public function testPasswordIsNotEMpty()
    {
        $user = new User();
        $user->setPassword('password');
        $this->assertNotEmpty($user->getPassword());
    }

    public function testUserIsNotEmpty()
    {
        $user = new User();
        $user->setPassword('password');
        $user->setEmail('email');
        $user->setRoles(['ROLE_USER']);

        $this->assertNotEmpty($user->getPassword());
        $this->assertNotEmpty($user->getEmail());
        $this->assertNotEmpty($user->getRoles());
    }

    public function testUserHasRoleUser()
    {
        $user = new User();
        $user->setPassword('password');
        $user->setEmail('email');

        $this->assertTrue(in_array('ROLE_USER', $user->getRoles()));
    }

    public function testHasUserIdentifier()
    {
        $user = new User();
        $user->setPassword('password');
        $user->setEmail('email');

        $this->assertEquals('email', $user->getUserIdentifier());
    }
}
