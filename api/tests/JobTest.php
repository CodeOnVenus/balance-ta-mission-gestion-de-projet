<?php

namespace App\Tests;

use App\Entity\Job;
use App\Entity\User;
use App\Entity\Techno;
use App\Entity\Recruiter;
use PHPUnit\Framework\TestCase;

class JobTest extends TestCase
{
    public function testJobEntity(): void
    {
        $job = (new Job())
                ->setDescription('description')
                ->setTJM(1000)
                ->setBeginAt(new \DateTimeImmutable("2021-01-01"))
                ->setCreatedAt(new \DateTimeImmutable("2021-01-01"))
                ->setReward('reward')
                ->setContact('contact')
                ->setLocalisation('localisation');

        $this->assertEquals('description', $job->getDescription());
        $this->assertEquals(1000, $job->getTJM());
        $this->assertEquals(new \DateTimeImmutable("2021-01-01"), $job->getBeginAt());
        $this->assertEquals(new \DateTimeImmutable("2021-01-01"), $job->getCreatedAt());
        $this->assertEquals('reward', $job->getReward());
        $this->assertEquals('contact', $job->getContact());
        $this->assertEquals('localisation', $job->getLocalisation());
    }

    public function testJobEntityHasTechno(): void
    {
        
        $techno = (new Techno())
                ->setName('name')
                ->setTags(['tag1', 'tag2'])
                ->setType('type');
        $job = (new Job())->addTechno($techno);

        $this->assertEquals('name', $job->getTechnos()[0]->getName());
        $this->assertEquals(['tag1', 'tag2'], $job->getTechnos()[0]->getTags());
        $this->assertEquals('type', $job->getTechnos()[0]->getType());
    }

    public function testJobEntityHasRecruiter(): void
    {
        $recruiter = (new Recruiter())
                ->setName('name')
                ->setCompany('company')
                ->setLinkedinLink('https://www.linkedin.com/in/firstname-lastname-123456789/');

        $job = (new Job())->setRecruiter($recruiter);

        $this->assertEquals('name', $job->getRecruiter()->getName());
        $this->assertEquals('company', $job->getRecruiter()->getCompany());
        $this->assertEquals('https://www.linkedin.com/in/firstname-lastname-123456789/', $job->getRecruiter()->getLinkedinLink());
    }

    public function testParticipantInstanceOfUser()
    {
    

        $user = (new User())
                ->setEmail('email')
                ->setRoles(['ROLE_USER'])
                ->setPassword('password');

        $job = (new Job())
                ->setDescription('description')
                ->setTJM(1000)
                ->setBeginAt(new \DateTimeImmutable("2021-01-01"))
                ->setCreatedAt(new \DateTimeImmutable("2021-01-01"))
                ->setReward('reward')
                ->setContact('contact')
                ->setLocalisation('localisation')
                ->addParticipant($user);

        $this->assertInstanceOf(User::class, $job->getParticipants()[0]);
    }

}
