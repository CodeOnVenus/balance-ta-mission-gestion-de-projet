SHELL := /bin/bash
install-back: 
		cd api; \
		composer install; \
		docker-compose up -d; \
		symfony console doctrine:database:create --if-not-exists; \
		symfony console d:m:m --no-interaction; \
		symfony console doctrine:fixture:load --no-interaction; \
		symfony serve -d; \
		symfony open:local
.PHONY: install-back

tests:
		./vendor/bin/phpunit --testdox
.PHONY: tests-back

cd : 
		cd ./api
		pwd
.PHONY: cd

